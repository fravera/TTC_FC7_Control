# TTC_FC7_Control

### Setup on CC7 (scroll down for instructions on setting up on SLC6)

1. Check which version of gcc is installed on your CC7, it should be > 4.8 (could be the default on CC7):

        $> gcc --version

2. On CC7 you also need to install boost v1.53 headers (default on this system) and pugixml as they don't ship with uHAL any more:

        $> sudo yum install boost-devel
        $> sudo yum install pugixml-devel

2. Install uHAL. SW tested with uHAL version up to 2.7.1

        Follow instructions from 
        https://ipbus.web.cern.ch/ipbus/doc/user/html/software/install/yum.html

3. Install CERN ROOT

        $> sudo yum install root
        $> sudo yum install root-net-http root-net-httpsniff  root-graf3d-gl root-physics root-montecarlo-eg root-graf3d-eve root-geom libusb-devel xorg-x11-xauth.x86_64


5. Install CMAKE > 2.8:

        $> sudo yum install cmake

### Install TTC_FC7_Control

        &> git clone https://gitlab.cern.ch/cms_tk_ph2/TTC_FC7_Control.git
        &> cd TTC_FC7_Control
        &> source setup.sh
        &> mkdir build
        &> cd build
        &> cmake ..
        &> make -j<number_of_cores>

### Available script (-h to get help)
        $> fpgaconfig

        



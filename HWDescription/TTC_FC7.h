/*!
        \file                TTC_FC7.h
        \brief               TTC_FC7 Description class, configs of the TTC_FC7
        \author              Lorenzo BIDEGAIN
        \date                14/07/14
        \version             1.0
        Support :            mail to : lorenzo.bidegain@gmail.com
 */

#ifndef TTC_FC7_TTC_FC7_h
#define TTC_FC7_TTC_FC7_h

#include "../Utils/easylogging++.h"
#include <map>
#include <stdint.h>
#include <vector>

/*!
 * \namespace TTC_FC7_HwDescription
 * \brief Namespace regrouping all the hardware description
 */
namespace TTC_FC7_HwDescription
{
    using TTC_FC7RegMap = std::map<std::string, uint32_t>; /*!< Map containing the registers of a board */

    /*!
 * \class TTC_FC7
 * \brief Read/Write TTC_FC7's registers on a file, handles a register map and handles a vector of Hybrid which are
 * connected to the TTC_FC7
 */
    class TTC_FC7
    {
    public:
        // C'tors: the TTC_FC7 only needs to know about which BE it is
        /*!
     * \brief Default C'tor
     */
        TTC_FC7();

        /*!
     * \brief Standard C'tor
     * \param pBeId
     */
        TTC_FC7(uint8_t pBeId);

        /*!
     * \brief C'tor for a standard TTC_FC7 reading a config file
     * \param pBeId
     * \param filename of the configuration file
     */
        TTC_FC7(uint8_t pBeId, const std::string &filename);

        /*!
     * \brief Destructor
     */
        ~TTC_FC7() {}

        // Public Methods

        /*!
     * \brief Get any register from the Map
     * \param pReg
     * \return The value of the register
     */
        uint32_t getReg(const std::string &pReg) const;


        uint32_t getId() const {return fBoardId;}

        /*!
     * \brief Set any register of the Map, if the register is not on the map, it adds it.
     * \param pReg
     * \param psetValue
     */
        void setReg(const std::string &pReg, uint32_t psetValue);

        // /*!
        // * \brief Get the Map of the registers
        // * \return The map of register
        // */
        TTC_FC7RegMap getTTC_FC7RegMap() const { return fRegMap; }

    private:
        TTC_FC7RegMap fRegMap; /*!< Map of TTC_FC7 Register Names vs. Register Values */
        uint32_t fBoardId;
    };
} // namespace TTC_FC7_HwDescription

#endif

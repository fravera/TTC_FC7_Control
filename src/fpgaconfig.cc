#include <cstdlib>
#include <inttypes.h>
#include <string>
#include <vector>

// #include "../HWDescription/BeBoard.h"
// #include "../HWInterface/BeBoardInterface.h"
#include "../System/TTC_FC7Controller.h"
#include "../Utils/ConsoleColor.h"
#include <boost/program_options.hpp> //!For command line arg parsing

using namespace TTC_FC7_HwDescription;
using namespace TTC_FC7_HwInterface;
using namespace TTC_FC7_System;

namespace po = boost::program_options;

INITIALIZE_EASYLOGGINGPP


/*!
************************************************
* Argument parser.
************************************************
*/

po::variables_map process_program_options(const int argc, const char* const argv[])
{
    po::options_description desc("Allowed options");

    desc.add_options()("help,h", "Print this help page")
    ("list,l"    , "Print the list of available firmware images on SD card (works only with CTA boards)")
    ("delete,d"  , po::value<std::string>()->default_value("default"), "Delete a firmware image on SD card (works only with CTA boards)")
    ("file,f"    , po::value<std::string>()->default_value("default"), "Local FPGA Bitstream file (*.mcs format for GLIB or *.bit/*.bin format for CTA boards)")
    ("download,o", po::value<std::string>()->default_value("default"), "Download an FPGA configuration from SD card to file (only for CTA boards)")
    ("config,c"  , po::value<std::string>()->default_value("default"), "Hw Description File")
    ("image,i"   , po::value<std::string>()->default_value("default"), "Without -f: load image from SD card to FPGA\nWith    -f: name of image written to SD card\n-f specifies the source filename");

    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
    }
    catch(po::error const& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    po::notify(vm);

    // Help
    if(vm.count("help"))
    {
        std::cout << desc << "\n";
        return 0;
    }

    return vm;
}


void verifyImageName(const std::string& strImage, const std::vector<std::string>& lstNames)
{
    if(lstNames.empty())
    {
        if(strImage.compare("1") != 0 && strImage.compare("2") != 0)
        {
            LOG(ERROR) << "Error, invalid image name, should be 1 (golden) or 2 (user)";
            exit(1);
        }
    }
    else
    {
        bool bFound = false;

        for(size_t iName = 0; iName < lstNames.size(); iName++)
        {
            if(!strImage.compare(lstNames[iName]))
            {
                bFound = true;
                break;
            }
        }

        if(!bFound)
        {
            LOG(ERROR) << "Error, this image name: " << strImage << " is not available on SD card";
            exit(1);
        }
    }
}



int main(int argc, char* argv[])
{
    auto* baseDirChar_p = std::getenv("TTC_FC7_BASE_DIR");
    if(baseDirChar_p == nullptr)
    {
        LOG(ERROR) << "Error, the environment variable TTC_FC7_BASE_DIR is not initialized (hint: source setup.sh)";
        exit(1);
    }

    el::Configurations conf(std::string(baseDirChar_p) + "/settings/logger.conf");
    el::Loggers::reconfigureAllLoggers(conf);

    TTC_FC7Controller cTTC_FC7Controller;

    boost::program_options::variables_map argumentMap = process_program_options(argc, argv);
    
    std::string        cHWFile = argumentMap["config"].as<std::string>();
    std::ostringstream cStr;
    cTTC_FC7Controller.InitializeHw(cHWFile, cStr);
    std::vector<std::string> lstNames = cTTC_FC7Controller.fTTC_FC7Interface->getFpgaConfigList();
    std::string              cFWFile;
    std::string              strImage("1");

    if(argumentMap.count("list"))
    {
        LOG(INFO) << lstNames.size() << " firmware images on SD card:";

        for(auto& name: lstNames) LOG(INFO) << " - " << name;

        exit(0);
    }
    else if(argumentMap.count("file"))
    {
        cFWFile = argumentMap["file"].as<std::string>();

        if(lstNames.size() == 0 && cFWFile.find(".mcs") == std::string::npos)
        {
            LOG(ERROR) << "Error, the specified file is not a .mcs file";
            exit(1);
        }
        else if(lstNames.size() > 0 && cFWFile.compare(cFWFile.length() - 4, 4, ".bit") && cFWFile.compare(cFWFile.length() - 4, 4, ".bin"))
        {
            LOG(ERROR) << "Error, the specified file is neither a .bit nor a .bin file";
            exit(1);
        }
    }
    else if(argumentMap.count("delete") && !lstNames.empty())
    {
        strImage = argumentMap["delete"].as<std::string>();
        verifyImageName(strImage, lstNames);
        cTTC_FC7Controller.fTTC_FC7Interface->DeleteFpgaConfig(strImage);
        LOG(INFO) << "Firmware image: " << strImage << " deleted from SD card";
        exit(0);
    }
    else if(!argumentMap.count("image"))
    {
        cFWFile = "";
        LOG(ERROR) << "Error, no FW image specified";
        exit(1);
    }

    if(argumentMap.count("image"))
    {
        strImage = argumentMap["image"].as<std::string>();

        if(!argumentMap.count("file"))
        {
            verifyImageName(strImage, lstNames);
            LOG(INFO) << BOLDBLUE << ">>> Done <<<" << RESET;
        }
    }
    else if(!lstNames.empty())
        strImage = "GoldenImage.bin";

    if(!argumentMap.count("file") && !argumentMap.count("download"))
    {
        cTTC_FC7Controller.fTTC_FC7Interface->JumpToFpgaConfig(strImage);
        exit(0);
    }

    bool cDone = 0;

    if(argumentMap.count("download"))
        cTTC_FC7Controller.fTTC_FC7Interface->DownloadFpgaConfig(strImage, argumentMap["download"].as<std::string>());
    else
        cTTC_FC7Controller.fTTC_FC7Interface->FlashProm(strImage, cFWFile.c_str());

    uint32_t progress;

    while(cDone == 0)
    {
        progress = cTTC_FC7Controller.fTTC_FC7Interface->GetConfiguringFpga()->getProgressValue();

        if(progress == 100)
        {
            cDone = 1;
            LOG(INFO) << BOLDBLUE << ">>> 100% Done <<<" << RESET;
        }
        else
        {
            LOG(INFO) << progress << "%  " << cTTC_FC7Controller.fTTC_FC7Interface->GetConfiguringFpga()->getProgressString() << "                 \r" << std::flush;
            sleep(1);
        }
    }
}

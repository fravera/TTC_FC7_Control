/*!
  \file                                            TTC_FC7Interface.h
  \brief                                           User Interface to the Boards
  \author                                          Lorenzo BIDEGAIN, Nicolas PIERRE
  \version                                         1.0
  \date                        31/07/14
  Support :                    mail to : lorenzo.bidegain@gmail.com, nicolas.pierre@cern.ch

*/


#ifndef TTC_FC7_TTC_FC7INTERFACE_H
#define TTC_FC7_TTC_FC7INTERFACE_H

#include <string>
#include <mutex>
#include <vector>
#include <uhal/uhal.hpp>

// #include "TTC_FC7FWInterface.h"
// #include "TTC_FC7.h"

namespace TTC_FC7_HwInterface
{
   class TTC_FC7FWInterface;
}
namespace TTC_FC7_HwDescription
{
   class TTC_FC7;
}
namespace FWUtils
{
   class FpgaConfig;
}

/*!
 * \namespace TTC_FC7_HwInterface
 * \brief Namespace regrouping all the interfaces to the hardware
 */

namespace TTC_FC7_HwInterface
{

    /*!
    * \class BeBoardInterface
    * \brief Class representing the User Interface to the different boards
    */
    class TTC_FC7Interface
    {
    private:
        TTC_FC7_HwInterface::TTC_FC7FWInterface* fTTC_FC7FWInterface;
        TTC_FC7_HwDescription::TTC_FC7*          fTTC_FC7;
        std::mutex theMtx;

    public:
        /*!
     * \brief Constructor of the TTC_FC7Interface class
     * \param pBoardMap Reference to the BoardFWInterface
     */
        TTC_FC7Interface(TTC_FC7_HwInterface::TTC_FC7FWInterface *pTTC_FC7FWInterface, TTC_FC7_HwDescription::TTC_FC7 *pTTC_FC7);

        /*!
     * \brief Destructor of the TTC_FC7Interface class
     */
        ~TTC_FC7Interface();

        /*!
     * \brief Update both Board register and Config File
     * \param TTC_FC7
     * \param pRegNode : Node of the register to update
     * \param pVal : Value to write
     */
        void WriteBoardReg(const std::string &pRegNode, const uint32_t &pVal);

        /*!
     * \brief Write a block of a given size into the board
     * \param TTC_FC7
     * \param pRegNode : Node of the register to write
     * pValVec Vector of values to write
     */
        void WriteBlockBoardReg(const std::string &pRegNode, const std::vector<uint32_t> &pValVec);

        /*!
     * \brief Write: Update both Board register and Config File
     * \param TTC_FC7
     * \param pRegVec : Vector of Register/Value pairs
     */
        void WriteBoardMultReg(const std::vector<std::pair<std::string, uint32_t>> &pRegVec);

        /*!
     * \brief Update Config File with the value in the Board register
     * \param TTC_FC7
     * \param pRegNode : Node of the register to update
     */
        uint32_t ReadBoardReg(const std::string &pRegNode);

        /*!
     * \brief Read a block of a given size from the board
     * \param TTC_FC7
     * \param pRegNode : Node of the register to read
     * \param pSize Number of 32-bit words in the block
     */
        std::vector<uint32_t> ReadBlockBoardReg(const std::string &pRegNode, uint32_t pSize);

        /*!
     * \brief Read a vector of Registers
     * \param TTC_FC7
     * \param pRegVec : Vector of Register/Value pairs
     */
        void ReadBoardMultReg(std::vector<std::pair<std::string, uint32_t>> &pRegVec);

        /*!
     * \brief Get the board infos
     * \param TTC_FC7
     */
        uint32_t getBoardInfo();

        /*!
     * \brief Configure the board with its Config File
     * \param TTC_FC7
     */
        void ConfigureBoard();

        /*!
     * \brief Start a DAQ
     * \param pBoard
     */
        void Start();

        /*!
     * \brief Stop a DAQ
     * \param TTC_FC7
     */
        void Stop();

        /*!
     * \brief Pause a DAQ
     * \param TTC_FC7
     */
        void Pause();

        /*!
     * \brief Resume a DAQ
     * \param TTC_FC7
     */
        void Resume();

        /*! \brief Get a uHAL node object from its path in the uHAL XML address file
     * \param TTC_FC7 pointer to a board description
     * \return Reference to the uhal::node object
     */
        const uhal::Node &getUhalNode(const std::string &pStrPath);

        /*! \brief Access to the uHAL main interface for a given board
     * \param TTC_FC7 pointer to a board description
     * \return pointer to the uhal::HwInterface object
     */
        uhal::HwInterface *getHardwareInterface();

        /*! \brief Access to the firmware interface for a given board
     * \return pointer to the BeBoardFWInterface object
     */
        TTC_FC7_HwInterface::TTC_FC7FWInterface *getFirmwareInterface() { return fTTC_FC7FWInterface; }

        /*! \brief Upload a configuration in a board FPGA
     * \param TTC_FC7 pointer to a board description
     * \param numConfig FPGA configuration number to be uploaded
     * \param pstrFile path to MCS file containing the FPGA configuration
     */
        void FlashProm(const std::string &strConfig, const char *pstrFile);

        /*! \brief Jump to an FPGA configuration
     * \param TTC_FC7 pointer to a board description
     * \param numConfig FPGA configuration number
     */
        void JumpToFpgaConfig(const std::string &strConfig);

        void DownloadFpgaConfig(const std::string &strConfig, const std::string &strDest);

        /*! \brief Current FPGA configuration
     * \param TTC_FC7 pointer to a board description
     * \return const pointer to an FPGA uploading process. NULL means that no upload is been processed.
     */
        const FWUtils::FpgaConfig *GetConfiguringFpga();

        /*! \brief Get the list of available FPGA configuration (or firmware images)
     * \param TTC_FC7 pointer to a board description */
        std::vector<std::string> getFpgaConfigList();

        /*! \brief Delete one Fpga configuration (or firmware image)
     * \param TTC_FC7 pointer to a board description
     * \param strId Firmware image identifier*/
        void DeleteFpgaConfig(const std::string &strId);

        /*! \brief Reboot the board */
        void RebootBoard();

        /*! \brief Set or reset the start signal */
        void SetForceStart(bool bStart);
    };
} // namespace TTC_FC7_HwInterface

#endif

/*!
\file                            TTC_FC7FWInterface.h
\brief                           BeBoardFWInterface base class of all type of boards
\author                          Lorenzo BIDEGAIN, Nicolas PIERRE
\version                         1.0
\date                            28/07/14
Support :                        mail to : lorenzo.bidegain@gmail.com, nico.pierre@icloud.com
*/

#ifndef TTC_FC7_BEBOARDFWINTERFACE_H
#define TTC_FC7_BEBOARDFWINTERFACE_H

#include "../HWDescription/TTC_FC7.h"
#include "../Utils/easylogging++.h"
#include "FpgaConfig.h"
#include "RegManager.h"

#include <uhal/uhal.hpp>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>

namespace TTC_FC7_HwInterface
{
    /*!
 * \class BeBoardFWInterface
 * \brief Class separating board system FW interface from uHal wrapper
 */
    class TTC_FC7FWInterface : public FWUtils::RegManager
    {

    public:
        /*!
     * \brief Constructor of the TTC_FC7FWInterface class
     * \param puHalConfigFileName : path of the uHal Config File*/
        TTC_FC7FWInterface(const char *puHalConfigFileName, uint32_t pBoardId);
        TTC_FC7FWInterface(const char *pId, const char *pUri, const char *pAddressTable);

        /*!
     * \brief Destructor of the TTC_FC7FWInterface class
     */
        virtual ~TTC_FC7FWInterface();

        /*! \brief Upload a configuration in a board FPGA */
        void FlashProm(const std::string &strConfig, const char *pstrFile);

        /*! \brief Jump to an FPGA configuration */
        void JumpToFpgaConfig(const std::string &strConfig);

        void DownloadFpgaConfig(const std::string &strConfig, const std::string &strDest);

        /*! \brief Current FPGA configuration*/
        const FWUtils::FpgaConfig *GetConfiguringFpga()  { return fFpgaConfig; }

        /*! \brief Get the list of available FPGA configuration (or firmware images)*/
        std::vector<std::string> getFpgaConfigList();

        /*! \brief Delete one Fpga configuration (or firmware image)*/
        void DeleteFpgaConfig(const std::string &strId);

        void checkIfUploading();

        void RebootBoard();

        std::vector<uint32_t> ReadBlockRegValue(const std::string &pRegNode, uint32_t pBlocksize);

        bool WriteBlockReg(const std::string& pRegNode, const std::vector<uint32_t>& pValues);



        /*!
     * \brief Configure the board with its Config File
     * \param pTTC_FC7
     */
        void ConfigureBoard(const TTC_FC7_HwDescription::TTC_FC7 *pTTC_FC7);

        /*!
     * \brief Start a DAQ
     */
        void Start();

        /*!
     * \brief Stop a DAQ
     */
        void Stop();

        /*!
     * \brief Pause a DAQ
     */
        void Pause();

        /*!
     * \brief Resume a DAQ
     */
        void Resume();

    private:
        FWUtils::FpgaConfig *fFpgaConfig;

    };
} // namespace TTC_FC7_HwInterface

#endif

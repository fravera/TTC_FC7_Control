/*
  FileName :                    TTC_FC7Interface.cc
  Content :                     User Interface to the Boards
  Programmer :                  Lorenzo BIDEGAIN, Nicolas PIERRE
  Version :                     1.0
  Date of creation :            31/07/14
  Support :                     mail to : lorenzo.bidegain@gmail.com nico.pierre@icloud.com
*/

#include "TTC_FC7Interface.h"
#include "TTC_FC7FWInterface.h"
#include "TTC_FC7.h"

using namespace TTC_FC7_HwDescription;
using namespace FWUtils;

namespace TTC_FC7_HwInterface
{
    TTC_FC7Interface::TTC_FC7Interface(TTC_FC7FWInterface *pTTC_FC7FWInterface, TTC_FC7 *pTTC_FC7) 
    : fTTC_FC7FWInterface(pTTC_FC7FWInterface) 
    , fTTC_FC7(pTTC_FC7)
    {}

    TTC_FC7Interface::~TTC_FC7Interface() {}

    void TTC_FC7Interface::WriteBoardReg(const std::string &pRegNode, const uint32_t &pVal)
    {
        fTTC_FC7FWInterface->WriteReg(pRegNode, pVal);
        fTTC_FC7->setReg(pRegNode, pVal);
    }

    void TTC_FC7Interface::WriteBlockBoardReg(const std::string &pRegNode, const std::vector<uint32_t> &pValVec)
    {
        fTTC_FC7FWInterface->WriteBlockReg(pRegNode, pValVec);
    }

    void TTC_FC7Interface::WriteBoardMultReg(const std::vector<std::pair<std::string, uint32_t>> &pRegVec)
    {
        fTTC_FC7FWInterface->WriteStackReg(pRegVec);
        for (const auto &cReg : pRegVec)
            fTTC_FC7->setReg(cReg.first, cReg.second);
    }

    uint32_t TTC_FC7Interface::ReadBoardReg(const std::string &pRegNode)
    {
        uint32_t cRegValue = static_cast<uint32_t>(fTTC_FC7FWInterface->ReadReg(pRegNode));
        fTTC_FC7->setReg(pRegNode, cRegValue);
        return cRegValue;
    }

    void TTC_FC7Interface::ReadBoardMultReg(std::vector<std::pair<std::string, uint32_t>> &pRegVec)
    {
        for (auto &cReg : pRegVec)
            try
            {
                cReg.second = static_cast<uint32_t>(fTTC_FC7FWInterface->ReadReg(cReg.first));
                fTTC_FC7->setReg(cReg.first, cReg.second);
            }
            catch (...)
            {
                std::cerr << "Error while reading: " + cReg.first;
                throw;
            }
    }

    std::vector<uint32_t> TTC_FC7Interface::ReadBlockBoardReg(const std::string &pRegNode, uint32_t pSize)
    {
        return fTTC_FC7FWInterface->ReadBlockRegValue(pRegNode, pSize);
    }

    void TTC_FC7Interface::ConfigureBoard()
    {
        std::lock_guard<std::mutex> theGuard(theMtx);

        fTTC_FC7FWInterface->ConfigureBoard(fTTC_FC7);
    }

    void TTC_FC7Interface::Start()
    {
        std::lock_guard<std::mutex> theGuard(theMtx);

        fTTC_FC7FWInterface->Start();
    }

    void TTC_FC7Interface::Stop()
    {
        std::lock_guard<std::mutex> theGuard(theMtx);

        fTTC_FC7FWInterface->Stop();
    }

    void TTC_FC7Interface::Pause()
    {
        std::lock_guard<std::mutex> theGuard(theMtx);

        fTTC_FC7FWInterface->Pause();
    }

    void TTC_FC7Interface::Resume()
    {
        std::lock_guard<std::mutex> theGuard(theMtx);

        fTTC_FC7FWInterface->Resume();
    }

    const uhal::Node &TTC_FC7Interface::getUhalNode(const std::string &pStrPath)
    {
        return fTTC_FC7FWInterface->getUhalNode(pStrPath);
    }

    uhal::HwInterface *TTC_FC7Interface::getHardwareInterface()
    {
        return fTTC_FC7FWInterface->getHardwareInterface();
    }

    void TTC_FC7Interface::FlashProm(const std::string &strConfig, const char *pstrFile)
    {
        fTTC_FC7FWInterface->FlashProm(strConfig, pstrFile);
    }

    void TTC_FC7Interface::JumpToFpgaConfig(const std::string &strConfig)
    {
        fTTC_FC7FWInterface->JumpToFpgaConfig(strConfig);
    }

    void TTC_FC7Interface::DownloadFpgaConfig(const std::string &strConfig, const std::string &strDest)
    {
        fTTC_FC7FWInterface->DownloadFpgaConfig(strConfig, strDest);
    }

    const FpgaConfig *TTC_FC7Interface::GetConfiguringFpga()
    {
        return fTTC_FC7FWInterface->GetConfiguringFpga();
    }

    std::vector<std::string> TTC_FC7Interface::getFpgaConfigList()
    {
        return fTTC_FC7FWInterface->getFpgaConfigList();
    }

    void TTC_FC7Interface::DeleteFpgaConfig(const std::string &strId)
    {
        fTTC_FC7FWInterface->DeleteFpgaConfig(strId);
    }

    void TTC_FC7Interface::RebootBoard()
    {
        fTTC_FC7FWInterface->RebootBoard();
    }

} // namespace TTC_FC7_HwInterface

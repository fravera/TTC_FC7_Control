/*
  FileName :                    TTC_FC7FWInterface.cc
  Content :                     TTC_FC7FWInterface base class of all type of boards
  Programmer :                  Lorenzo BIDEGAIN, Nicolas PIERRE
  Version :                     1.0
  Date of creation :            31/07/14
  Support :                     mail to : lorenzo.bidegain@gmail.com, nico.pierre@icloud.com
*/

#include "TTC_FC7FWInterface.h"
#include "FpgaConfig.h"

using namespace FWUtils;
using namespace TTC_FC7_HwDescription;

namespace TTC_FC7_HwInterface
{
    TTC_FC7FWInterface::TTC_FC7FWInterface(const char *puHalConfigFileName, uint32_t pBoardId) 
    : RegManager(puHalConfigFileName, pBoardId)
    {
        fFpgaConfig = new FpgaConfig(this);
        std::cout<<fFpgaConfig<<std::endl;
    }

    TTC_FC7FWInterface::TTC_FC7FWInterface(const char *pId, const char *pUri, const char *pAddressTable)
    : RegManager(pId, pUri, pAddressTable)
    {
        fFpgaConfig = new FpgaConfig(this);
    }

    TTC_FC7FWInterface::~TTC_FC7FWInterface()
    {
        delete fFpgaConfig;
        fFpgaConfig = nullptr;
    }

    void TTC_FC7FWInterface::FlashProm(const std::string &strConfig, const char *pstrFile)
    {
        checkIfUploading();

        fFpgaConfig->runUpload(strConfig, pstrFile);
    }

    void TTC_FC7FWInterface::JumpToFpgaConfig(const std::string &strConfig)
    {
        checkIfUploading();

        fFpgaConfig->jumpToImage(strConfig);
    }

    void TTC_FC7FWInterface::DownloadFpgaConfig(const std::string &strConfig, const std::string &strDest)
    {
        checkIfUploading();
        fFpgaConfig->runDownload(strConfig, strDest.c_str());
    }

    std::vector<std::string> TTC_FC7FWInterface::getFpgaConfigList()
    {
        checkIfUploading();
        return fFpgaConfig->getFirmwareImageNames();
    }

    void TTC_FC7FWInterface::DeleteFpgaConfig(const std::string &strId)
    {
        checkIfUploading();
        fFpgaConfig->deleteFirmwareImage(strId);
    }

    void TTC_FC7FWInterface::checkIfUploading()
    {
        if(fFpgaConfig->getUploadingFpga() > 0)
            throw std::runtime_error("This board is uploading an FPGA configuration");
    }

    void TTC_FC7FWInterface::RebootBoard()
    {
        fFpgaConfig->resetBoard();
    }

    std::vector<uint32_t> TTC_FC7FWInterface::ReadBlockRegValue(const std::string &pRegNode, uint32_t pBlocksize)
    {
        return ReadBlockReg(pRegNode, pBlocksize);
    }

    bool TTC_FC7FWInterface::WriteBlockReg(const std::string& pRegNode, const std::vector<uint32_t>& pValues)
    {
        bool cWriteCorr = RegManager::WriteBlockReg(pRegNode, pValues);
        // std::this_thread::sleep_for (std::chrono::microseconds (fWait_us) );
        return cWriteCorr;
    }

    void TTC_FC7FWInterface::ConfigureBoard(const TTC_FC7 *pTTC_FC7)
    {
        // to be implemented
    }
    
    void TTC_FC7FWInterface::Start()
    {
        // to be implemented
    }

    void TTC_FC7FWInterface::Stop()
    {
        // to be implemented
    }

    void TTC_FC7FWInterface::Pause()
    {
        // to be implemented
    }

    void TTC_FC7FWInterface::Resume()
    {
        // to be implemented
    }

} // namespace TTC_FC7_HwInterface

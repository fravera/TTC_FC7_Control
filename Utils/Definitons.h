#ifndef TTC_FC7_Definitions
#define TTC_FC7_Definitions

#include <unordered_map>
#include <string>

namespace TTC_FC7_Utils
{
    using SettingsMap = std::unordered_map<std::string, double>; /*!< Maps the settings */
}

#endif
/*!
  \file                  TTC_FC7Controller.cc
  \brief                 Controller of the System, overall wrapper of the framework
  \author                Mauro DINARDO
  \version               2.0
  \date                  01/01/20
  Support:               email to mauro.dinardo@cern.ch
*/

#include "TTC_FC7Controller.h"

using namespace TTC_FC7_HwDescription;
using namespace TTC_FC7_HwInterface;
using namespace TTC_FC7_Utils;

namespace TTC_FC7_System
{
    TTC_FC7Controller::TTC_FC7Controller()
        : fTTC_FC7Interface(nullptr), fTTC_FC7FWInterface(nullptr), fTTC_FC7(nullptr)
    {
    }

    TTC_FC7Controller::~TTC_FC7Controller()
    {
        Destroy();
    }

    void TTC_FC7Controller::Destroy()
    {
        delete fTTC_FC7Interface;
        fTTC_FC7Interface = nullptr;
        delete fTTC_FC7FWInterface;
        fTTC_FC7FWInterface = nullptr;
        delete fTTC_FC7;
        fTTC_FC7 = nullptr;

        LOG(INFO) << BOLDRED << ">>> TTC_FC7Controller  destroyed <<<" << RESET;
    }

    void TTC_FC7Controller::InitializeHw(const std::string &pFilename, std::ostream &os)
    {
        this->fParser.parseHW(pFilename, fTTC_FC7FWInterface, fTTC_FC7, os);
        fTTC_FC7Interface = new TTC_FC7Interface(fTTC_FC7FWInterface, fTTC_FC7);
        std::vector<std::string> lstNames = fTTC_FC7Interface->getFpgaConfigList();

    }

    void TTC_FC7Controller::InitializeSettings(const std::string &pFilename, std::ostream &os)
    {
        this->fParser.parseSettings(pFilename, fSettingsMap, os);
    }

    void TTC_FC7Controller::ConfigureHw()
    {
        fTTC_FC7Interface->ConfigureBoard();
    }

    void TTC_FC7Controller::Stop()
    {
        fTTC_FC7Interface->Stop();
    }

    void TTC_FC7Controller::Pause()
    {
        fTTC_FC7Interface->Pause();
    }

    void TTC_FC7Controller::Resume()
    {
        fTTC_FC7Interface->Resume();
    }

    void TTC_FC7Controller::Abort()
    {
        LOG(ERROR) << BOLDRED << __PRETTY_FUNCTION__ << " Abort not implemented" << RESET;
    }

    double TTC_FC7Controller::findValueInSettings(const std::string name, double defaultValue) const
    {
        auto setting = fSettingsMap.find(name);
        return (setting != std::end(fSettingsMap) ? setting->second : defaultValue);
    }

} // namespace TTC_FC7_System

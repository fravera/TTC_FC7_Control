/*!
  \file                  TTC_FC7Controller.h
  \brief                 Controller of the TTC_FC7, overall wrapper of the framework
  \author                Mauro DINARDO
  \version               2.0
  \date                  01/01/20
  Support:               email to mauro.dinardo@cern.ch
*/

#ifndef TTC_FC7_TTC_FC7CONTROLLER_H
#define TTC_FC7_TTC_FC7CONTROLLER_H

#include "../HWDescription/TTC_FC7.h"
#include "../HWInterface/TTC_FC7FWInterface.h"
#include "../HWInterface/TTC_FC7Interface.h"
#include "FileParser.h"

/*!
 * \namespace TTC_FC7_System
 * \brief Namespace regrouping the framework wrapper
 */
namespace TTC_FC7_System
{

    /*!
 * \class TTC_FC7Controller
 * \brief Create, initialise, configure a predefined HW structure
 */
    class TTC_FC7Controller
    {
    public:
        TTC_FC7_HwInterface::TTC_FC7Interface *fTTC_FC7Interface;
        TTC_FC7_HwInterface::TTC_FC7FWInterface *fTTC_FC7FWInterface;
        TTC_FC7_HwDescription::TTC_FC7 *fTTC_FC7;
        TTC_FC7_Utils::SettingsMap fSettingsMap;

        /*!
     * \brief Constructor of the TTC_FC7Controller class
     */
        TTC_FC7Controller();

        /*!
     * \brief Destructor of the TTC_FC7Controller class
     */
        virtual ~TTC_FC7Controller();

        /*!
     * \brief Destroy the TTC_FC7Controller object: clear the HWDescription Objects, FWInterface etc.
     */
        void Destroy();

        /*!
     * \brief Initialize the Hardware via a config file
     * \param pFilename : HW Description file
     *\param os         : ostream to dump output
     */
        void InitializeHw(const std::string &pFilename, std::ostream &os = std::cout);

        /*!
     * \brief Initialize the settings
     * \param pFilename : settings file
     *\param os         : ostream to dump output
     */
        void InitializeSettings(const std::string &pFilename, std::ostream &os = std::cout);

        /*!
     * \brief Configure the Hardware with XML file indicated values
     */
        void ConfigureHw();

        void Start(int runNumber);
        void Stop();
        void Pause();
        void Resume();
        void Configure(std::string cHWFile);
        void Abort();

        double findValueInSettings(const std::string name, double defaultValue = 0.) const;

    private:
        FileParser fParser;
    };
} // namespace Ph2_TTC_FC7

#endif

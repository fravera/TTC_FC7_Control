#include "FileParser.h"

using namespace TTC_FC7_HwDescription;
using namespace TTC_FC7_HwInterface;
using namespace TTC_FC7_Utils;

namespace TTC_FC7_System
{
    void FileParser::parseHW(const std::string &pFilename, TTC_FC7FWInterface*& pTTC_FC7FWInterface, TTC_FC7*& pTTC_FC7, std::ostream &os)
    {
        if (pFilename.find(".xml") != std::string::npos)
        {
            parseHWxml(pFilename, pTTC_FC7FWInterface, pTTC_FC7, os);
        }
        else
        {
            LOG(ERROR) << BOLDRED << "Could not parse settings file " << pFilename << " - it is not .xml" << RESET;
        }
    }

    void FileParser::parseSettings(const std::string &pFilename, SettingsMap &pSettingsMap, std::ostream &os)
    {
        if (pFilename.find(".xml") != std::string::npos)
            parseSettingsxml(pFilename, pSettingsMap, os);
        else
        {
            LOG(ERROR) << BOLDRED << "Could not parse settings file " << pFilename << " - it is not .xm" << RESET;
        }
    }

    void FileParser::parseHWxml(const std::string &pFilename, TTC_FC7FWInterface*& pTTC_FC7FWInterface, TTC_FC7*& pTTC_FC7, std::ostream &os)
    {
        int i, j;

        pugi::xml_document doc;
        pugi::xml_parse_result result;

        result = doc.load_file(pFilename.c_str());
        
        if (!result)
        {
            os << BOLDRED << "ERROR :\n Unable to open the file : " << RESET << pFilename << std::endl;
            os << BOLDRED << "Error description : " << RED << result.description() << RESET << std::endl;

            throw std::runtime_error("Unable to parse XML source!");
            return;
        }

        os << RESET << "\n\n";

        for (i = 0; i < 80; i++)
            os << "*";
        os << "\n";

        for (j = 0; j < 35; j++)
            os << " ";
        os << BOLDRED << "HW SUMMARY" << RESET << std::endl;

        for (i = 0; i < 80; i++)
            os << "*";
        os << "\n";

        const std::string strUhalConfig = expandEnvironmentVariables(doc.child("HwDescription").child("Connections").attribute("name").value());

        this->parseTTC_FC7(doc.child("HwDescription").child("TTC_FC7"), pTTC_FC7FWInterface, pTTC_FC7, os);

        

        for (i = 0; i < 80; i++)
            os << "*";

        os << "\n";

        for (j = 0; j < 32; j++)
            os << " ";

        os << BOLDRED << "END OF HW SUMMARY" << RESET << std::endl;

        for (i = 0; i < 80; i++)
            os << "*";

        os << std::endl;
    }

    void FileParser::parseTTC_FC7(pugi::xml_node pBeBordNode, TTC_FC7FWInterface*& pTTC_FC7FWInterface, TTC_FC7*& pTTC_FC7, std::ostream &os)
    {
        uint32_t cBeId = pBeBordNode.attribute("Id").as_int();

        pTTC_FC7 = new TTC_FC7(cBeId);

        pugi::xml_node cBeBoardConnectionNode = pBeBordNode.child("connection");

        std::string cId = cBeBoardConnectionNode.attribute("id").value();
        std::string cUri = cBeBoardConnectionNode.attribute("uri").value();
        std::string cAddressTable = expandEnvironmentVariables(cBeBoardConnectionNode.attribute("address_table").value());

        pTTC_FC7FWInterface = new TTC_FC7FWInterface(cId.c_str(), cUri.c_str(), cAddressTable.c_str());
        
        os << BOLDCYAN << "|"
           << "       "
           << "|"
           << "----"
           << "Board Id:      " << BOLDYELLOW << cId << std::endl
           << BOLDCYAN << "|"
           << "       "
           << "|"
           << "----"
           << "URI:           " << BOLDYELLOW << cUri << std::endl
           << BOLDCYAN << "|"
           << "       "
           << "|"
           << "----"
           << "Address Table: " << BOLDYELLOW << cAddressTable << std::endl
           << RESET;

        for (pugi::xml_node cBeBoardRegNode = pBeBordNode.child("Register"); cBeBoardRegNode; cBeBoardRegNode = cBeBoardRegNode.next_sibling())
        {
            if (std::string(cBeBoardRegNode.name()) == "Register")
            {
                std::string cNameString;
                double cValue;
                this->parseRegister(cBeBoardRegNode, cNameString, cValue, pTTC_FC7, os);
            }
        }
    }

    void FileParser::parseRegister(pugi::xml_node pRegisterNode, std::string &pAttributeString, double &pValue, TTC_FC7*& pTTC_FC7, std::ostream &os)
    {
        if (std::string(pRegisterNode.name()) == "Register")
        {
            if (std::string(pRegisterNode.first_child().value()).empty())
            {
                if (!pAttributeString.empty())
                    pAttributeString += ".";

                pAttributeString += pRegisterNode.attribute("name").value();

                for (pugi::xml_node cNode = pRegisterNode.child("Register"); cNode; cNode = cNode.next_sibling())
                {
                    std::string cAttributeString = pAttributeString;
                    this->parseRegister(cNode, cAttributeString, pValue, pTTC_FC7, os);
                }
            }
            else
            {
                if (!pAttributeString.empty())
                    pAttributeString += ".";

                pAttributeString += pRegisterNode.attribute("name").value();
                pValue = convertAnyDouble(pRegisterNode.first_child().value());
                os << GREEN << "|\t|\t|"
                   << "----" << pAttributeString << ": " << BOLDYELLOW << pValue << RESET << std::endl;
                pTTC_FC7->setReg(pAttributeString, pValue);
            }
        }
    }

    void FileParser::parseSettingsxml(const std::string &pFilename, SettingsMap &pSettingsMap, std::ostream &os)
    {
        pugi::xml_document doc;
        pugi::xml_parse_result result;

        result = doc.load_file(pFilename.c_str());
        
        if (result == false)
        {
            os << BOLDRED << "ERROR : Unable to open the file " << RESET << pFilename << std::endl;
            os << BOLDRED << "Error description: " << RED << result.description() << RESET << std::endl;

            throw std::runtime_error("Unable to parse XML source!");
        }

        for (pugi::xml_node nSettings = doc.child("HwDescription").child("Settings"); nSettings == doc.child("HwDescription").child("Settings"); nSettings = nSettings.next_sibling())
        {
            os << std::endl;

            for (pugi::xml_node nSetting = nSettings.child("Setting"); nSetting; nSetting = nSetting.next_sibling())
            {
                pSettingsMap[nSetting.attribute("name").value()] = convertAnyDouble(nSetting.first_child().value());
                os << BOLDRED << "Setting" << RESET << " -- " << BOLDCYAN << nSetting.attribute("name").value() << RESET << ":" << BOLDYELLOW << convertAnyDouble(nSetting.first_child().value()) << RESET
                   << std::endl;
            }
        }
    }

    std::string FileParser::expandEnvironmentVariables(std::string s)
    {
        if(s.find("${") == std::string::npos) return s;

        std::string pre  = s.substr(0, s.find("${"));
        std::string post = s.substr(s.find("${") + 2);

        if(post.find('}') == std::string::npos) return s;

        std::string variable = post.substr(0, post.find('}'));
        std::string value    = "";

        post = post.substr(post.find('}') + 1);

        if(getenv(variable.c_str()) != NULL) value = std::string(getenv(variable.c_str()));

        return expandEnvironmentVariables(pre + value + post);
    }


    double FileParser::convertAnyDouble(const char* pRegValue)
    {
        int         baseType = 0;
        std::string myRegValue(pRegValue);
        if(myRegValue.find("0x") != std::string::npos)
            baseType = 16;
        else if(myRegValue.find("0d") != std::string::npos)
            baseType = 10;
        else if(myRegValue.find("0b") != std::string::npos)
            baseType = 2;
        if(baseType != 0) myRegValue.erase(0, 2);
        return strtod(myRegValue.c_str(), 0);
    }
}

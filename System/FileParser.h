/*!
  \file                    FileParser.h
  \brief                   Class to parse configuration files
  \author                  Georg Auzinger
  \version                 1.0
  \date                    01/07/2016
  Support :                mail to : georg.auzinger@SPAMNOT.cern.sh
*/

#ifndef TTC_FC7_FILEPARSER_H
#define TTC_FC7_FILEPARSER_H

#include "../HWDescription/TTC_FC7.h"
#include "../HWInterface/TTC_FC7FWInterface.h"
#include "../Utils/ConsoleColor.h"
#include "../Utils/Definitons.h"
#include "../Utils/easylogging++.h"

#include "pugixml.hpp"
#include <iostream>
#include <stdlib.h>
#include <string>
#include <unordered_map>
#include <vector>

/*!
 * \namespace TTC_FC7_System
 * \brief Namespace regrouping the framework wrapper
 */
namespace TTC_FC7_System
{
    /*!
 * \class FileParser
 * \brief parse a predefined HW structure
 */
    class FileParser
    {
    public:
        FileParser() {}
        ~FileParser() {}

        void parseHW(const std::string &pFilename, TTC_FC7_HwInterface::TTC_FC7FWInterface*& pTTC_FC7FWInterface, TTC_FC7_HwDescription::TTC_FC7*& pTTC_FC7, std::ostream &os);
        void parseSettings(const std::string &pFilename, TTC_FC7_Utils::SettingsMap &pSettingsMap, std::ostream &os);

    private:
        /*!
        * \brief Initialize the hardware via  XML config file
        * \param pFilename : HW Description file
        *\param os : ostream to dump output
        */
        void parseHWxml(const std::string &pFilename, TTC_FC7_HwInterface::TTC_FC7FWInterface*& pTTC_FC7FWInterface, TTC_FC7_HwDescription::TTC_FC7*& pTTC_FC7, std::ostream &os);

        /*!
        * \brief Initialize the hardware via xml config file
        * \param pFilename : HW Description file
        *\param os : ostream to dump output
        */
        void parseSettingsxml(const std::string &pFilename, TTC_FC7_Utils::SettingsMap &pSettingsMap, std::ostream &os);

        void parseTTC_FC7(pugi::xml_node pBeBordNode, TTC_FC7_HwInterface::TTC_FC7FWInterface*& pTTC_FC7FWInterface, TTC_FC7_HwDescription::TTC_FC7*& pTTC_FC7, std::ostream &os);
        void parseRegister(pugi::xml_node pRegisterNode, std::string &pAttributeString, double &pValue, TTC_FC7_HwDescription::TTC_FC7*& pTTC_FC7, std::ostream &os);
        std::string expandEnvironmentVariables(std::string s);
        double convertAnyDouble(const char* pRegValue);

    };
} // namespace TTC_FC7_System

#endif
